  SUMMARY_URL=$(grep "\"SummaryUrl\":" polaris_logs.log | sed 's/^.*: //'| sed -e 's/^"//' -e 's/"$//')
  PROJECT_NAME=`echo $BITBUCKET_REPO_FULL_NAME  | tr '[:upper:]' '[:lower:]'`
  PROJECT_VERSION=`echo $BITBUCKET_BRANCH  | tr '[:upper:]' '[:lower:]'`

  if [ -n "$SUMMARY_URL" ]; then
    echo "Summary URL ~> $SUMMARY_URL"
  else
    echo "Summary URL not found so trying with project name & branch."
    SUMMARY_URL="null"
  fi

  url_escape() {
    echo `echo $1 | sed s:{:%7B:g | sed s:}:%7D:g`
  }

  REPORT_URL=`url_escape "https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER_UUID/$BITBUCKET_REPO_UUID/commit/$BITBUCKET_COMMIT/reports/1"`
  ANNOTATIONS_URL=`url_escape "https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER_UUID/$BITBUCKET_REPO_UUID/commit/$BITBUCKET_COMMIT/reports/1/annotations/1"`

  curl -X DELETE -u synopsys-sig:$APP_PASSWORD $REPORT_URL
  curl -X DELETE -u synopsys-sig:$APP_PASSWORD $ANNOTATIONS_URL

  echo $BITBUCKET_REPO_OWNER_UUID
  echo $BITBUCKET_REPO_UUID
  echo $BITBUCKET_COMMIT
  echo $BITBUCKET_REPO_OWNER

  echo $PROJECT_NAME
  echo $PROJECT_VERSION

  APP_PASSWORD=${APP_PASSWORD:?'APP_PASSWORD is needed to run code insight read more in the documentation.'}

  echo $POLARIS_SERVER_URL
  echo $POLARIS_ACCESS_TOKEN

  java -jar build/libs/bitbucket-polaris-reporting-all-1.0-SNAPSHOT.jar \
  $BITBUCKET_REPO_OWNER_UUID \
  $BITBUCKET_REPO_UUID \
  $BITBUCKET_COMMIT \
  $BITBUCKET_REPO_OWNER \
  $APP_PASSWORD \
  $POLARIS_SERVER_URL \
  $POLARIS_ACCESS_TOKEN \
  $PROJECT_NAME \
  $PROJECT_VERSION \
  $SUMMARY_URL

  echo "\n Finished Uploading!"