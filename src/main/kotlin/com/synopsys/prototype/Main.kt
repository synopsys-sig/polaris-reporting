package com.synopsys.prototype

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.squareup.okhttp.*
import com.synopsys.integration.exception.IntegrationException
import com.synopsys.integration.log.IntLogger
import com.synopsys.integration.log.LogLevel
import com.synopsys.integration.log.PrintStreamIntLogger
import com.synopsys.integration.polaris.common.configuration.PolarisServerConfigBuilder
import com.synopsys.integration.polaris.common.model.IssueResourcesSingle
import com.synopsys.integration.polaris.common.request.PolarisRequestFactory
import com.synopsys.integration.polaris.common.rest.AccessTokenPolarisHttpClient
import com.synopsys.integration.polaris.common.service.PolarisService
import com.synopsys.prototype.model.bitbucket.Annotation
import com.synopsys.prototype.model.bitbucket.Report
import com.synopsys.prototype.model.polaris.eventsource.EventSource
import com.synopsys.prototype.model.polaris.issue.PolarisIssue
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

const val externalId = "synopsysPolaris-001"

fun main(args: Array<String>){

    var repoOwnerUuid = args[0].replace("{","").replace("}", "")
    var repoUuid = args[1].replace("{","").replace("}", "")
    val commitId = args[2]
    val repoOwner = args[3]
    val appPassword = args[4]
    val polarisUrl = args[5]
    val polarisToken = args[6]
    val projectName = args[7]
    val projectBranch = args[8]
    val summaryURL = args[9]

    repoOwnerUuid = "%7B$repoOwnerUuid%7D"
    repoUuid = "%7B$repoUuid%7D"
    println("OWNER_UUID: $repoOwnerUuid, UUID: $repoUuid, commit_id: $commitId")

    // Setup
    val builder = PolarisServerConfigBuilder()
    builder.url = polarisUrl
    builder.accessToken = polarisToken
    val polarisServerConfig = builder.build()

    val logger: IntLogger = PrintStreamIntLogger(System.out, LogLevel.INFO)
    val polarisHttpClient = polarisServerConfig.createPolarisHttpClient(logger)
    val polarisServicesFactory = polarisServerConfig.createPolarisServicesFactory(logger)

    val polarisService = polarisServicesFactory.createPolarisService()
    val projectService = polarisServicesFactory.createProjectService()
    val branchService = polarisServicesFactory.createBranchService()
    val issueService = polarisServicesFactory.createIssueService()

    val report = Report()
    report.reportType = "SECURITY"
    report.title = "Synopsys SAST Report"
    report.details = "Manage security, quality, and license compliance risk that comes from the use of open source and third-party code in applications and containers."
    report.reporter = "Coverity on Polaris"
    report.result = "FAILED"

    // date
    val tz = TimeZone.getTimeZone("UTC")
    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'") // Quoted "Z" to indicate UTC
    df.timeZone = tz
    val nowAsISO = df.format(Date())
    report.createdOn = nowAsISO
    report.updatedOn = nowAsISO

    var projectId: String? = null
    var branchId: String? = null

    if (summaryURL != "null" || summaryURL != "") {
        val project: Pair<String, String> = parseSummaryURL(summaryURL)
        projectId = project.first
        branchId = project.second
        println("Summary URL valid & branch ID $branchId present for project ID $projectId in the Polaris instance $polarisUrl")
    } else {
        val project = projectService.getProjectByName(projectName)
        if (project.isPresent) {
            println("Project $projectName present in the Polaris instance $polarisUrl")
            projectId = project.get().id
            val branch = branchService.getBranchForProjectByName(projectId, projectBranch)
            if (branch.isPresent) {
                println("Branch $projectBranch present for project $projectName in the Polaris instance $polarisUrl")
                branchId = branch.get().id
            } else {
                println("Branch not present in the Project $projectName for the Polaris instance $polarisUrl")
            }
        } else {
            println("Project $projectName not present in the Polaris instance $polarisUrl")
        }
    }

    if (projectId.isNullOrBlank() || branchId.isNullOrBlank()) {
        println("Project & Branch not present in the Polaris instance $polarisUrl")
        return
    }

    val allIssues = issueService.getIssuesForProjectAndBranch(projectId, branchId)
    report.link = "$polarisUrl/projects/$projectId/branches/$branchId"
    report.logoUrl = "https://www.cloudfoundry.org/wp-content/uploads/icon_synopsys@2x.png"

    // printing report
    val gsonPretty = GsonBuilder().setPrettyPrinting().create()
    val reportAsString = gsonPretty.toJson(report)
    val fileName = "polaris-report.json"
    val file = File(fileName)
    file.writeText(reportAsString)
    println("---------------------------------------------------- REPORT JSON DUMPED ---------------------------------------------------------")

    // posting report
    putReportRequest(
        reportAsString,
        repoOwnerUuid,
        repoUuid,
        commitId,
        repoOwner,
        appPassword
    )

    // Annotations
    val annotations = mutableListOf<Annotation>()

    for (issue in allIssues) {
        val annotation =
            Annotation()
        annotation.annotationType = "VULNERABILTIY"

        val severityIssue =
            getIssueForProjectBranchAndIssueKeyWithSeverityIncluded(
                polarisHttpClient,
                polarisService,
                projectId,
                branchId,
                issue.attributes.issueKey
            )
        val gson = Gson()
        val polarisIssue = gson.fromJson(severityIssue.json, PolarisIssue::class.java)
        val severity = polarisIssue.data?.relationships?.severity?.data?.id?.toUpperCase()
        val findingKey = issue.attributes.findingKey
        var runId = ""

        for (issueDetails in polarisIssue.included!!) {
            if (issueDetails.type == "issue-type") {
                var annotationDetails = issueDetails.attributes?.description
                if (issueDetails.attributes?.localEffect != null){
                    annotationDetails += issueDetails.attributes?.localEffect
                }
                annotation.severity = severity
                annotation.summary = issueDetails.attributes?.name
                annotation.details = annotationDetails
            }


            if (issueDetails.type == "transition") {
                val revisionId = issueDetails.attributes?.revisionId
                val linkToIssue = "$polarisUrl/projects/$projectId/branches/$branchId/revisions/$revisionId/issues/${issue.attributes.issueKey}"
                runId = issueDetails.attributes?.runId!!
                annotation.link = linkToIssue
            }

            if (issueDetails.type == "path") {
                if (issueDetails.attributes != null) {
                    if (issueDetails.attributes!!.path != null) {
                        annotation.path = issueDetails.attributes!!.path!!.joinToString(separator = "/")
                        val locationURL = issueDetails.attributes!!.path!!.joinToString(separator = "%2f")
                        val eventWithSource =
                            getEventWithSourceForProjectBranchAndIssueKey(
                                polarisHttpClient,
                                findingKey,
                                runId,
                                locationURL
                            )
                        if (eventWithSource.data != null) {
                            for (datum in eventWithSource.data!!) {
                                val lineNumber = datum.mainEventLineNumber
                                annotation.line = lineNumber

                                if(datum.mainEventFilePath != null)
                                annotation.path = datum.mainEventFilePath!!.joinToString(separator = "/")
                            }
                        }
                    }
                }
            }
        }


        annotations.add(annotation)
    }

    // printing annotations
    val annotationsAsString = gsonPretty.toJson(annotations)
    val annotationFileName = "polaris-annotation.json"
    val annotationFile = File(annotationFileName)
    annotationFile.writeText(annotationsAsString)
    println("---------------------------------------------------- ANNOTATION JSON DUMPED ---------------------------------------------------------")


    // posting annotations
    annotations.forEachIndexed { index, annotation ->
        val annotationAsString = gsonPretty.toJson(annotation)
        putAnnotationRequest(
            annotationAsString,
            repoOwnerUuid,
            repoUuid,
            commitId,
            index + 1,
            repoOwner,
            appPassword
        )
    }
    println("---------------------------------------------------- ANNOTATION POSTED ---------------------------------------------------------")


}

fun parseSummaryURL(summaryURL: String): Pair<String, String> {
    val branchId = summaryURL.substringAfter("branches/")
    val projectId= summaryURL.substringAfter("projects/").substringBefore("/branches/$branchId")
    return Pair(projectId, branchId)
}

private val SINGLE_ISSUE_RESOURCES: TypeToken<IssueResourcesSingle> = object : TypeToken<IssueResourcesSingle>() {}

@Throws(IntegrationException::class)
fun getIssueForProjectBranchAndIssueKeyWithSeverityIncluded(
    polarisHttpClient: AccessTokenPolarisHttpClient,
    polarisService: PolarisService,
    projectId: String,
    branchId: String,
    issueKey: String
): IssueResourcesSingle {
    val uri: String = polarisHttpClient.polarisServerUrl + PolarisService.GET_ISSUE_API_SPEC(issueKey) + "?include%5Bissue%5D%5B%5D=severity"
    val requestBuilder: com.synopsys.integration.rest.request.Request.Builder =
        createRequestBuilder(
            uri,
            projectId,
            branchId
        )
    val request = requestBuilder.build()
    return polarisService.get(SINGLE_ISSUE_RESOURCES.type, request)
}

@Throws(IntegrationException::class)
fun getEventWithSourceForProjectBranchAndIssueKey(
    polarisHttpClient: AccessTokenPolarisHttpClient,
    findingKey: String,
    runId: String,
    locatorPath: String
): EventSource {
    val uri: String = polarisHttpClient.polarisServerUrl + "/api/code-analysis/v0/events-with-source"
    val requestBuilder: com.synopsys.integration.rest.request.Request.Builder =
        createEventSourceRequestBuilder(
            uri,
            findingKey,
            runId,
            locatorPath
        )
    val request = requestBuilder.build()
    val response = polarisHttpClient.execute(request)
    return Gson().fromJson(response.contentString, EventSource::class.java)
}

fun createRequestBuilder(
    uri: String,
    projectId: String,
    branchId: String
): com.synopsys.integration.rest.request.Request.Builder {
    return PolarisRequestFactory.createDefaultRequestBuilder()
        .addQueryParameter(PolarisService.PROJECT_ID, projectId)
        .addQueryParameter(PolarisService.BRANCH_ID, branchId)
        .uri(uri)
}

fun createEventSourceRequestBuilder(
    uri: String,
    findingKey: String,
    runId: String,
    locatorPath: String
): com.synopsys.integration.rest.request.Request.Builder {
    return PolarisRequestFactory.createDefaultRequestBuilder()
        .addQueryParameter("finding-key", findingKey)
        .addQueryParameter("run-id", runId)
        .addQueryParameter("locator-path", locatorPath)
        .mimeType("application/json")
        .uri(uri)
}

fun putReportRequest(data: String?, repoOwnerUuid: String, repoUuid: String, commitId: String, repoOwner: String, appPassword: String) {
    val client = OkHttpClient()
    val mediaType: MediaType = MediaType.parse("application/json")
    val body: RequestBody = RequestBody.create(mediaType, data.toString())

    val userpass = "$repoOwner:$appPassword" //"STLMZjS4hfkuXL4xe8MH"
    val basicAuth = "Basic " + userpass.base64encoded
    val url = "https://api.bitbucket.org/2.0/repositories/$repoOwnerUuid/$repoUuid/commit/$commitId/reports/$externalId"
    println("URL: $url")
    val request: Request = Request.Builder()
        .url(url)
        .put(body)
        .addHeader("Content-Type", "application/json")
        .addHeader("Authorization", basicAuth)
        .build()

    val response: Response = client.newCall(request).execute()
    println("Response ~> code: ${response.code()}, message: ${response.message()}, body: ${response.body().string()}")
}

fun putAnnotationRequest(data: String?, repoOwnerUuid: String, repoUuid: String, commitId: String, count: Int, repoOwner: String, appPassword: String) {
    val client = OkHttpClient()
    val mediaType: MediaType = MediaType.parse("application/json")
    val body: RequestBody = RequestBody.create(mediaType, data.toString())

    val userpass = "$repoOwner:$appPassword"  //"STLMZjS4hfkuXL4xe8MH"
    val basicAuth = "Basic " + userpass.base64encoded
    val url = "https://api.bitbucket.org/2.0/repositories/$repoOwnerUuid/$repoUuid/commit/$commitId/reports/$externalId/annotations/$count"
    // println("URL: $url")
    val request: Request = Request.Builder()
        .url(url)
        .put(body)
        .addHeader("Content-Type", "application/json")
        .addHeader("Authorization", basicAuth)
        .build()

    val response: Response = client.newCall(request).execute()
    println("Response ~> code: ${response.code()}, message: ${response.message()}, body: ${response.body().string()}")
}

/**
 * Base64 encode a string.
 */
private const val BASE64_SET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
val String.base64encoded: String get() {
    val pad  = when (this.length % 3) {
        1 -> "=="
        2 -> "="
        else -> ""
    }
    var raw = this
    (1 .. pad.length).forEach { _ -> raw += 0.toChar() }
    return StringBuilder().apply {
        (raw.indices step 3).forEach { it ->
            val n: Int = (0xFF.and(raw[ it ].toInt()) shl 16) +
                    (0xFF.and(raw[it+1].toInt()) shl  8) +
                    0xFF.and(raw[it+2].toInt())
            listOf( (n shr 18) and 0x3F,
                (n shr 12) and 0x3F,
                (n shr  6) and 0x3F,
                n      and 0x3F).forEach { append(BASE64_SET[it]) }
        }
    }   .dropLast(pad.length)
        .toString() + pad
}