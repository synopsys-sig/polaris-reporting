package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PolarisIssue {
    @SerializedName("data")
    @Expose
    var data: Data? = null
    @SerializedName("included")
    @Expose
    var included: List<Included>? = null

}