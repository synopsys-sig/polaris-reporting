package com.synopsys.prototype.model.polaris.eventsource

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SourceAfter {
    @SerializedName("start-line")
    @Expose
    var startLine: Int? = null
    @SerializedName("end-line")
    @Expose
    var endLine: Int? = null
    @SerializedName("source-code")
    @Expose
    var sourceCode: String? = null

}