package com.synopsys.prototype.model.polaris.eventsource

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Datum {
    @SerializedName("run-id")
    @Expose
    var runId: String? = null
    @SerializedName("finding-key")
    @Expose
    var findingKey: String? = null
    @SerializedName("main-event-file-path")
    @Expose
    var mainEventFilePath: List<String>? = null
    @SerializedName("main-event-line-number")
    @Expose
    var mainEventLineNumber: Int? = null
    @SerializedName("language")
    @Expose
    var language: String? = null
    @SerializedName("example-events-caption")
    @Expose
    var exampleEventsCaption: String? = null
    @SerializedName("example-events-groups")
    @Expose
    var exampleEventsGroups: List<Any>? = null
    @SerializedName("events")
    @Expose
    var events: List<Event>? = null
    @SerializedName("type")
    @Expose
    var type: String? = null

}
