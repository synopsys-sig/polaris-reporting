package com.synopsys.prototype.model.polaris.eventsource

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class EventSource {
    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null

}