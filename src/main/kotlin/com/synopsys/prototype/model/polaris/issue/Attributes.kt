package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Attributes {
    @SerializedName("finding-key")
    @Expose
    var findingKey: String? = null
    @SerializedName("issue-key")
    @Expose
    var issueKey: String? = null
    @SerializedName("sub-tool")
    @Expose
    var subTool: String? = null

}
