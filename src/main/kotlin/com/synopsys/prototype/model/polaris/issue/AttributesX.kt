package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AttributesX {
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("version")
    @Expose
    var version: String? = null
    @SerializedName("issue-type")
    @Expose
    var issueType: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("abbreviation")
    @Expose
    var abbreviation: String? = null
    @SerializedName("extra")
    @Expose
    var extra: Extra? = null
    @SerializedName("transition-type")
    @Expose
    var transitionType: String? = null
    @SerializedName("cause")
    @Expose
    var cause: String? = null
    @SerializedName("transition-date")
    @Expose
    var transitionDate: String? = null
    @SerializedName("branch-id")
    @Expose
    var branchId: String? = null
    @SerializedName("revision-id")
    @Expose
    var revisionId: String? = null
    @SerializedName("run-id")
    @Expose
    var runId: String? = null
    @SerializedName("path")
    @Expose
    var path: List<String>? = null
    @SerializedName("path-type")
    @Expose
    var pathType: String? = null
    @SerializedName("local-effect")
    @Expose
    var localEffect: String? = null
}