package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Relationships {
    @SerializedName("path")
    @Expose
    var path: Path? = null
    @SerializedName("tool-domain-service")
    @Expose
    var toolDomainService: ToolDomainService? = null
    @SerializedName("issue-type")
    @Expose
    var issueType: IssueType? = null
    @SerializedName("tool")
    @Expose
    var tool: Tool? = null
    @SerializedName("latest-observed-on-run")
    @Expose
    var latestObservedOnRun: LatestObservedOnRun? = null
    @SerializedName("transitions")
    @Expose
    var transitions: List<Transition>? = null
    @SerializedName("related-taxa")
    @Expose
    var relatedTaxa: List<Any>? = null
    @SerializedName("related-indicators")
    @Expose
    var relatedIndicators: List<Any>? = null
    @SerializedName("severity")
    @Expose
    var severity: Severity? = null

}