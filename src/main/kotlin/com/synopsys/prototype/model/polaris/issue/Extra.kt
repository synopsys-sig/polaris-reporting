package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Extra {
    @SerializedName("rank")
    @Expose
    var rank: String? = null

}