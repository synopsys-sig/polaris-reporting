package com.synopsys.prototype.model.polaris.eventsource

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Event {
    @SerializedName("covlstr-event-description")
    @Expose
    var covlstrEventDescription: String? = null
    @SerializedName("event-description")
    @Expose
    var eventDescription: String? = null
    @SerializedName("event-number")
    @Expose
    var eventNumber: Int? = null
    @SerializedName("event-set")
    @Expose
    var eventSet: Int? = null
    @SerializedName("event-tag")
    @Expose
    var eventTag: String? = null
    @SerializedName("event-tree-position")
    @Expose
    var eventTreePosition: String? = null
    @SerializedName("event-type")
    @Expose
    var eventType: String? = null
    @SerializedName("line-number")
    @Expose
    var lineNumber: Int? = null
    @SerializedName("source-after")
    @Expose
    var sourceAfter: SourceAfter? = null
    @SerializedName("path")
    @Expose
    var path: List<String>? = null
    @SerializedName("filePath")
    @Expose
    var filePath: String? = null
    @SerializedName("evidence-events")
    @Expose
    var evidenceEvents: List<Any>? = null
    @SerializedName("source-before")
    @Expose
    var sourceBefore: Any? = null

}