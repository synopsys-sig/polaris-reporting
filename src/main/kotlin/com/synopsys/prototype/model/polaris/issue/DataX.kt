package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class DataX {
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null

}