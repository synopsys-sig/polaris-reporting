package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Data {
    @SerializedName("attributes")
    @Expose
    var attributes: Attributes? = null
    @SerializedName("relationships")
    @Expose
    var relationships: Relationships? = null
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("links")
    @Expose
    var links: Links? = null
    @SerializedName("type")
    @Expose
    var type: String? = null

}