package com.synopsys.prototype.model.polaris.eventsource

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Meta {
    @SerializedName("limit")
    @Expose
    var limit: Int? = null
    @SerializedName("offset")
    @Expose
    var offset: Int? = null
    @SerializedName("total")
    @Expose
    var total: Int? = null

}