package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Tool {
    @SerializedName("data")
    @Expose
    var data: DataX? = null

}