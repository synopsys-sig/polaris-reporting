package com.synopsys.prototype.model.polaris.issue

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Included {
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("attributes")
    @Expose
    var attributes: AttributesX? = null
    @SerializedName("id")
    @Expose
    var id: String? = null

}