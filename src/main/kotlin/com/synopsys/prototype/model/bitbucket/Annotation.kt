package com.synopsys.prototype.model.bitbucket

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Annotation {
    @SerializedName("annotation_type")
    @Expose
    var annotationType: String? = null
    @SerializedName("summary")
    @Expose
    var summary: String? = null
    @SerializedName("details")
    @Expose
    var details: String? = null
    @SerializedName("severity")
    @Expose
    var severity: String? = null
    @SerializedName("link")
    @Expose
    var link: String? = null
    @SerializedName("line")
    @Expose
    var line: Int? = null
    @SerializedName("path")
    @Expose
    var path: String? = null
}