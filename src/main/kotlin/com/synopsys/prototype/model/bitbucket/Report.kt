package com.synopsys.prototype.model.bitbucket

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Report {
    @SerializedName("report_type")
    @Expose
    var reportType: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("details")
    @Expose
    var details: String? = null
    @SerializedName("result")
    @Expose
    var result: String? = null
    @SerializedName("reporter")
    @Expose
    var reporter: String? = null
    @SerializedName("link")
    @Expose
    var link: String? = null
    @SerializedName("logo_url")
    @Expose
    var logoUrl: String? = null
    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("created_on")
    @Expose
    var createdOn: String? = null
    @SerializedName("updated_on")
    @Expose
    var updatedOn: String? = null

}